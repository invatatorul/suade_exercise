FROM base/archlinux:latest
MAINTAINER ACIOBANI <aciokkan@gmail.com>

RUN pacman -Syu --noconfirm
RUN pacman -S git file awk gcc libxslt vim --noconfirm

RUN pacman -S python3 python-pip python-lxml --noconfirm
RUN pip install --upgrade pip
ADD . /suade_exercise

WORKDIR /suade-exercise
ENV PYTHONPATH "${PYTHONPATH}:/suade_exercise"

RUN pip3 install -r requirements.txt
RUN pwd
RUN echo $PYTHONPATH

CMD ["echo `ls .`"]

ENTRYPOINT ["python3.6"]

CMD ["python suade_src/run_v1.py"]
