SUADE RES (Report Exporting Service)

This service provides a Web API interface to generte PDF/XML reports, from abstract DB schemas based on a given ID.
The service will produce the reports and depending on the need, they can be delivered via email, or downloaded directly

Proposed technologies:
   - reportlab for PDFs;
   - RabbiitMQ for queueing ;
   - python3, requests, XML, psycopg (for web api interface?)
   - Docker

API:
   - for this purpose we'll need a single secure endpoint that'll trigger a worker in the background.
   - the worker's job is to queue jobs in RabbitMQ, that'll get consumed at a later date.
   - this mechanism is trying to be scallable and also offload the API from becoming unresponsive.
   - to ensure protection against DDoS and other similar attacks we can add a proxy layer on top of the app, and extra security measures on the network layer.
   - invalid API key should return 404.
   - invalid ID validation.
   - invalid response from the event listener.
   - considering the fact that we have an external" dependency we need to validate the ID requested.

Event Listener:
   - this will hold a Producer / Consumer interface.
   - Producer will be called when an API request is being made
   - Consumer will consume each event and trigger a Reporter to generate a PDF;
   - as part of the Consumer, there will be a request to get the DB schema as a WEB request (will reuse some personal code here for ease of logging); 
      - should the ID be invalid a response will invalidate the initial request.
   - validations need to be considered here:
      - invalid request to get the schema can break the flow
      - invalid data in the schema can break the flow, etc

PostgresWebRequester (silly name, but does the job for now):
   - will be a small unified interface to deal with the Postgres API Interface(?) - GET method (?)

Reporter:
   - this is an abstract layer on top of reportlab library, and will generate reports based of of some templates.
   - this is a bigger chunk of work as templates need to be designed and written.
   - validations need to be considered here, as any invalid input has the potential of breaking the chain.

Considerations and drawbacks:
   - need to remember reportlab (it's been a while since I've used it)
   - how to interface with the postgres api interface provided?
   - we need to focus on adding some degree of testing (hard to balance)?
   - how do we feed the reports back to the user/requester?
   - how does the postgres suade response looks??
   - 3hrs at hand and only 2 hrs reamining;

Goal: 
   - finish it all, realistically, need to have a working demo

Issues:
   - ran into issues with reportlab installation (24m)
   - had to read something about reportlab (1h)
   - design and 'document' somewhat of a solution to proposed exercise (1h)
   - no tests written because lost track of time and got drawn into building the actual solution

INSTALLATION:
   - clone the repo
   - cd into repo
   - run: 
      - docker build -t suade_exercise:v0.1 .
      - docker run -it -d -p 5000:5000 suade_exercise:v0.1

