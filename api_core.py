# -*- coding: utf-8 -*-
# author: ACIOBANI

from base import Base

from furl import furl

import requests
from requests.auth import HTTPBasicAuth
from requests.exceptions import ConnectionError


REQUEST_IS = '{method} - {url}\t{data}\t{json}'
RESPONSE_IS = '{method} - Response is: {response}'


class ApiRequestsBase(Base):
    def __init__(self, auth_user, auth_api_key, **kwargs):
        """
        Perform the required initialisation for the class
        """
        session = kwargs.get('session')

        super(ApiRequestsBase, self).__init__()

        self._auth = self._generate_basic_auth(auth_user,
                                               auth_api_key)
        if session:
            self.req = requests.session()
        else:
            self.req = requests

    @staticmethod
    def _generate_basic_auth(auth_user, auth_api_key):

        auth = HTTPBasicAuth(auth_user, auth_api_key)

        return auth

    def update_basic_auth(self, **kwargs):
        """
        Update basic auth with new credentials.
        :param kwargs: auth_user & auth_api_key
        :return: None
        """
        self.log('Current basic auth: user: {user} - password: {password}'.format(user=self._auth.username,
                                                                                  password=self._auth.password))
        auth_user = kwargs.get('auth_user', self._auth.username)
        auth_api_key = kwargs.get('auth_api_key', self._auth.password)

        if auth_user is None:
            auth_user = self._auth.username

        if auth_api_key is None:
            auth_api_key = self._auth.password

        self._auth = HTTPBasicAuth(auth_user, auth_api_key)

        self.log('Updated basic auth: user: {user} - password: {password}'.format(user=self._auth.username,
                                                                                  password=self._auth.password))

    def post(self, url, data=None, json='', **kwargs):
        """
        Wrapper over requests library to send POST request.
        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
        :param json: (optional) json data to send in the body of the :class:`Request`.
        :param kwargs: Optional arguments that ``request`` takes.
        :return: request response
        :rtype: dict
        """

        result = {}
        self.log(REQUEST_IS.format(method='POST',
                                   url=url,
                                   data=data,
                                   json=json))
        try:
            response = self.req.post(url,
                                     data=data,
                                     json=json,
                                     auth=self._auth,
                                     **kwargs)

            result.update(self._convert_to_json(response))
        except ConnectionError as ce:
            result = {'error': ce}

        self.log(RESPONSE_IS.format(method='POST',
                                    response=result))
        return result

    def put(self, url, data=None, json='', **kwargs):
        """
        Wrapper over requests library to send POST request.
        :param url: URL for the new :class:`Request` object.
        :param data: (optional) Dictionary, bytes, or file-like object to send in the body of the :class:`Request`.
        :param json: (optional) json data to send in the body of the :class:`Request`.
        :param kwargs: Optional arguments that ``request`` takes.
        :return: request response
        :rtype: dict
        """

        result = {}

        self.log(REQUEST_IS.format(method='PUT',
                                   url=url,
                                   data=data,
                                   json=json))

        try:
            response = self.req.put(url,
                                    data=data,
                                    json=json,
                                    auth=self._auth,
                                    **kwargs)

            result.update(self._convert_to_json(response))
        except ConnectionError as ce:
            result = {'error': ce}

        self.log(RESPONSE_IS.format(method='PUT',
                                    response=result))
        return result

    def get(self, url, params=None, **kwargs):
        """
        Wrapper over requests library to send GET request.

        :param url: URL for the new :class:`Request` object.
        :param params: (optional) Dictionary or bytes to be sent in the query string for the :class:`Request`.
        :param kwargs: Optional arguments that ``request`` takes.
        :return: request response
        :rtype: dict
        """

        result = {}

        self.log(REQUEST_IS.format(method='GET', url=url, data=kwargs or params, json=''))

        try:
            response = self.req.get(url,
                                    params=params,
                                    auth=self._auth,
                                    **kwargs)
            result.update(self._convert_to_json(response))
        except ConnectionError as ce:
            result = {'error': ce}

        self.log(RESPONSE_IS.format(method='GET',
                                    response=result))
        return result

    def get_without_auth(self, url, params=None, **kwargs):
        """
        Wrapper over requests library to send GET request.

        :param url: URL for the new :class:`Request` object.
        :param params: (optional) Dictionary or bytes to be sent in the query string for the :class:`Request`.
        :param kwargs: Optional arguments that ``request`` takes.
        :return: request response
        :rtype: dict
        """

        result = {}

        self.log(REQUEST_IS.format(method='GET', url=url, data=kwargs, json=''))

        try:
            response = self.req.get(url,
                                    params=params,
                                    **kwargs)

            result.update(self._convert_to_json(response))
        except ConnectionError as ce:
            result = {'error': ce}

        self.log(RESPONSE_IS.format(method='GET',
                                    response=result))
        return result

    def list(self, url, payload=None, l_filter=None, **kwargs):
        """
        Wrapper over get requests library to extract the list request.

        :param url: URL for the new :class:`Request` object.
        :param payload: limit and page object
        :param l_filter: filter objects by
        :param kwargs: Optional arguments that ``request`` takes.
        :return: request response
        :rtype: dict
        """
        full_list = []
        # default page
        page = 1

        while True:
            if payload:
                page = payload.get('page')
                self.log("Page from payload {}".format(page))
                payload_url = furl(url).add(payload)
            else:
                payload_url = url

            self.log("Fetching page: {}".format(page))

            resp = self.get(payload_url, params=l_filter)

            # Check correct status code
            if resp.get('status_code') is not 200:
                self.log("Invalid response returned. Response data: {}".format(resp))
                return resp

            # Check the number of pages
            if resp.get('pages') == 0:
                self.log("Invalid list returned. Pages: {}".format(resp.get('pages')))
                return resp['_embedded']['items']

            full_list += resp['_embedded']['items']

            self.log("Page {} of {}".format(resp.get('page'), resp.get('pages')))
            # Check if all pages read
            if resp.get('pages') == resp.get('page'):
                break

            page += 1
            payload['page'] = page

        self.log("Full item list: {}".format(full_list))
        return full_list

    def delete(self, url, **kwargs):
        """
        Wrapper over requests library to send DELETE request.

        :param url: URL for the new :class:`Request` object.
        :param kwargs: Optional arguments that ``request`` takes.
        :return: request response
        :rtype: dict
        """

        result = {}

        self.log(REQUEST_IS.format(method='DELETE', url=url, data=kwargs, json=''))

        try:
            response = self.req.delete(url,
                                       auth=self._auth,
                                       **kwargs)

            result.update(self._convert_to_json(response))
        except ConnectionError as ce:
            result = {'error': ce}

        self.log(RESPONSE_IS.format(method='DELETE',
                                    response=result))
        return result

    def _convert_to_json(self, response):
        """
        Convert requests `Response` class to JSON format,
        or meaningful JSON object in case of failures.
        """
        result = {}
        try:
            json_response = response.json()

            # because few endpoints on the Cabin return a list instead of a json dict, with pagination.
            if isinstance(json_response, list):
                self.log('Converting list to dict.'.format(json_response))
                result['_embedded'] = {'items': json_response}
            elif isinstance(json_response, dict):
                if json_response.get('code') and 'error' in json_response:
                    json_response.pop('code')
                result.update(json_response)

            result['status_code'] = response.status_code
        except ValueError as _:
            # Value error will occur when an HTML repsonse is received rather than JSON
            self.log('HTML response received: {} Exception type: {}'.format(response.text, 'ValueError'))

        except AttributeError as _:
            self.log("AttributeError")
            if isinstance(response, str):
                result = {'error': _,
                          'text': response,
                          'code': 523}
                return result
            else:
                self.log('Error: {}\nCannot parse response: {}'.format(_, response))

        return result
