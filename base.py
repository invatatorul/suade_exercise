# -*- coding: utf-8 -*-
# author: ACIOBANI

import inspect
import os

from base_logger import BaseLogger


class Base(BaseLogger):
    """
    Base class, that can be extended to meet the purpose of the testing framework.
    """

    def __init__(self):
        the_method = ''
        the_class = ''

        stack = inspect.stack()
        my_locals = stack[1][0].f_locals

        if '__class__' in my_locals:
            the_class = my_locals.get('self').__class__.__name__
            if 'methodName' in my_locals:
                the_method = my_locals.get('methodName')
        elif '__spec__' in my_locals:
            # the_class = my_locals.get('__spec__').name.split('.')[-1]
            pckg = my_locals.get('__package__')
            if pckg:
                the_class = pckg.split('.')[-1]
            else:
                the_class = os.path.basename(my_locals.get('__file__'))[:-3]
            spec = my_locals.get('__spec__')
            if spec:
                the_method = spec.name.split('.')[-1]
            else:
                the_method = my_locals.get('__name__')

        logger_name = '{class_}'
        if the_method:
            logger_name += ' - {method}'.format(method=the_method)
        caller = logger_name.format(class_=the_class)

        super(Base, self).__init__(logger=caller)

    @staticmethod
    def get_caller_frame(skip=1):
        stack = inspect.stack()
        start = 0 + skip
        if len(stack) < start + 1:
            return ''
        parent_frame = stack[start][0]

        caller = []
        module = inspect.getmodule(parent_frame)

        if module:
            caller.append(module.__name__)
        # detect class name
        if 'self' in parent_frame.f_locals:
            caller.append(parent_frame.f_locals['self'].__class__.__name__)
        codename = parent_frame.f_code.co_name
        if codename != '<module>':  # top level
            caller.append(codename)  # method
        del parent_frame

        return caller[-1]
