# -*- coding: UTF-8 -*-
# author: 'ACIOBANI'

import os
import logging
import logging.config as config
from logging import StreamHandler
import time

import yaml

# disable requests.packages.urllib3 INFO logging messages:
logging.getLogger("requests").setLevel(logging.WARNING)

LOGGING_YAML = """
version: 1
disable_existing_loggers: False
formatters:
    simple:
        format: "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
        datefmt: "%Y-%m-%d %H:%M:%S"
handlers:
    console:
        class: logging.StreamHandler
        level: DEBUG
        formatter: simple
        stream: ext://sys.stdout
    base_info_handler:
        class: logging.handlers.TimedRotatingFileHandler
        when: midnight
        formatter: simple
        encoding: utf8
        filename: will_be_replaced
loggers:
    BaseLogger:
        level: DEBUG
        handlers: [console]
        propagate: yes
root:
    level: INFO
    handlers: [console]
"""


class BaseLogger(object):
    def __init__(self, logger='BaseLogger'):
        """
        Scaffolding for the logger
        :logger: Name of the caller
        """
        self._set_up_config(logger)
        self.logger = logging.getLogger(logger)
        if self.logger.handlers:
            self.logger.handlers.pop(0)

    def add_handler(self, handler):
        """
        Create new handler. Currently StreamHandler supported.
        :param handler: StreamHandler
        """
        # TODO: refactor in case we need a different handler.
        str_handler = StreamHandler(handler)

        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s', "%Y-%m-%d %H:%M:%S")
        str_handler.setFormatter(formatter)

        self.logger.addHandler(str_handler)

    def log(self, data, level='info', trace=False):
        """
        Logs the data to the requested level, with or without a stack-trace
        :param data: string The data to be logged
        :param level: string The level at which to log the data
        :param trace: bool Whether or not to log the stack-trace as well
        :return: void
        """

        getattr(self.logger, level)(data, exc_info=trace)

    @staticmethod
    def _replace_file_name(config_file, logger_name=''):
        """
        Sets the name of the log file to the current date, makes things easier to read in the long run
        Not sure if its possible to do this in the config file itself?
        :param config_file dictionary The loaded configuration file
        :return: Dictionary
        """
        log_path = os.path.normpath(os.path.dirname(os.path.realpath(__file__)) + "/../logs/")

        # TODO - Try/except for failure to create log directory

        # Check if the log directory exists and try to create if not
        if not os.path.exists(log_path):
            os.makedirs(log_path)

        config_path = os.path.normpath(os.path.dirname(
            os.path.realpath(__file__)) + "/../logs/" + str(time.strftime("%Y%m%d_fwk_log")) + ".log")

        config_file['handlers']['base_info_handler']['filename'] = config_path
        if logger_name:
            config_file['loggers'][logger_name] = config_file['loggers']['BaseLogger']

        return config_file

    def _set_up_config(self, logger_name=''):
        """
        Configures the logger
        """

        # if os.path.exists(self.path):
        #     with open(self.path, 'rt') as f:
        config_file = self._replace_file_name(yaml.load(LOGGING_YAML),
                                              logger_name=logger_name)
        logging.config.dictConfig(config_file)
        # else:
        #     self._set_basic_config(self.path)

    @staticmethod
    def _set_basic_config(path):
        """
        Sets the config if no yaml file is available
        """
        logging.basicConfig(filename=path,
                            level=logging.DEBUG,
                            format='%(asctime)s - %(name)s - %(levelname)s - %(message)s')

    def _set_logger_path(self, path):
        """
        Set up the logger
        """
        if not path:
            path = os.path.dirname(os.path.realpath(__file__))
            path += '/../config/logging.yaml'
            self.path = os.path.normpath(path)
        else:
            self.path = path
