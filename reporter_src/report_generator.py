# -*- coding: utf-8 -*-
# author: ACIOBANI
import json

from reportlab.platypus import SimpleDocTemplate, Spacer, Table, TableStyle
from reportlab.lib.pagesizes import A4
from reportlab.lib.units import inch
from reportlab.lib import colors

GRID_STYLE = TableStyle(
    [('GRID', (0, 0), (-1, -1), 0.25, colors.white),
     ('ALIGN', (-1, -1), (-1, -1), 'LEFT')])

PAGE_HEIGHT = A4[1]
PAGE_WIDTH = A4[0]

TIMES_BOLD = 'Times-Bold'
TIMES_ROMAN = 'Times-Roman'


class ReportGenerator(object):
    def __init__(self):
        all_reports = [(1,
                        '{"organization":"Dunder Mifflin","reported_at":"2015-04-21","created_at":"2015-04-22",'
                        '"inventory":[{"name":"paper","price":"2.00"}, '
                        '{"name":"stapler","price":"5.00"},{"name":"printer","price":"125.00"},'
                        '{"name":"ink","price":"3000.00"}]}'),
                       (2,
                        '{"organization":"Skynet Papercorp","reported_at":"2015-04-22","created_at":"2015-04-23",'
                        '"inventory":[{"name":"paper","price":"4.00"}]}')]
        self.my_report_data = json.loads(all_reports[0][1])

    def draw_report(self, canvas, *args):
        # organization:
        organization_str = 'Organization: {}'.format(self.my_report_data['organization'])

        # reported:
        reported_str = 'Reported: {}'.format(self.my_report_data['reported_at'])

        # created:
        created_str = 'Created: {}'.format(self.my_report_data['created_at'])

        canvas.saveState()
        # Header
        canvas.setFont(TIMES_BOLD, 18)
        canvas.drawCentredString(PAGE_WIDTH / 2.0, PAGE_HEIGHT - inch * 2, 'The Report')

        canvas.setFont(TIMES_BOLD, 6)
        canvas.drawString(PAGE_WIDTH / 4, PAGE_HEIGHT - inch * 2.5, organization_str)
        canvas.drawString(PAGE_WIDTH / 4, PAGE_HEIGHT - inch * 2.7, reported_str)
        canvas.drawString(PAGE_WIDTH / 4, PAGE_HEIGHT - inch * 2.9, created_str)

        # Footer - refactor/reuse  fonts
        canvas.setFont(TIMES_ROMAN, 9)
        canvas.drawString(0.5 * inch, 0.75 * inch, 'First Page')
        canvas.restoreState()

    def write_pdf(self):
        # Generate the PDF
        doc = SimpleDocTemplate('suade_reports.pdf',
                                pagesize=A4,
                                leftMargin=0.5 * inch,
                                rightMargin=0.5 * inch,
                                bottomMargin=1.5 * inch)
        drawer = [Spacer(1, 2 * inch)]
        # Add the table - refactor widths, sizes
        col_widths = (PAGE_WIDTH / 2 - (2 * inch), PAGE_WIDTH / 2 - (2 * inch))
        row_heights = (16, 16, 16, 16)

        data = [tuple(item.values()) for item in self.my_report_data['inventory']]
        table = Table(data, col_widths, row_heights)
        table.setStyle(GRID_STYLE)
        drawer.append(table)
        drawer.append(Spacer(1, 0.10 * inch))
        doc.build(drawer, onFirstPage=self.draw_report)
