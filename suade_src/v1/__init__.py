# -*- coding: utf-8 -*-
# author: ACIOBANI

from flask import Flask, render_template

from suade_src.v1.endpoints.reports.controller import reports_bp

app_v1 = Flask(__name__)

app_v1.config.from_object('config_v1')


# Register blueprints
app_v1.register_blueprint(reports_bp)


@app_v1.errorhandler(404)
def not_found(_):
    return render_template('404.html'), 404
