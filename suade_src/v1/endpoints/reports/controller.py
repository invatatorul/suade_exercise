# -*- coding: utf-8 -*-
# author: ACIOBANI

from flask import jsonify, request, Blueprint

# need to add authentication

reports_bp = Blueprint('reports', __name__, url_prefix='/reports')


# what validations can we do here?
@reports_bp.route('/<report_id>', methods=['GET'])
def queue_report(report_id):
    """
    Add a report ID in a queue.
    :param report_id: report id to search and generate report for
    :return: None?
    """
    report_success_msg = 'Report {id} has been queued.'
    # we're assuming for now, that the id's passed in are integers
    try:
        try:
            report_id = int(report_id)
        except ValueError as _:
            raise TypeError('Invalid ID - {}! Should be integer.'.format(report_id))
        return jsonify({'status_code': 200,
                        'message': report_success_msg.format(id=report_id)})
    except TypeError as _:
        return jsonify({'status_code': 500,
                        'message': _.args[0]}), 500
